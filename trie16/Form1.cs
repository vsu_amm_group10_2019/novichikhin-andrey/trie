﻿using System;
using System.Windows.Forms;
using System.IO;

namespace trie16
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Trie tree = new Trie();

        private void taskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tree.MakeReverse();
            tree.MakeTree(treeView1);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Для заданного trie-дерева построить trie-дерево, в котором все слова расположены в обратном порядке");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] lines = File.ReadAllLines(openFileDialog1.FileName);
                foreach (string str in lines)
                {
                    string[] words = str.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string word in words)
                    {
                        tree.Add(word.Trim().ToLower());
                    };
                }
                tree.MakeTree(treeView1);
            }
        }

        private void addWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Word wordForm = new Word();
            wordForm.ShowDialog();
            if (!string.IsNullOrEmpty(wordForm.word))
            {
                tree.Add(wordForm.word.Trim().ToLower());
                tree.MakeTree(treeView1);
            }
        }

        private void deleteWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Word wordForm = new Word();
            wordForm.ShowDialog();
            if (!string.IsNullOrEmpty(wordForm.word))
            {
                if (tree.Delete(wordForm.word.ToLower()))
                {
                    tree.MakeTree(treeView1);
                }
                else
                {
                    MessageBox.Show("Слово не найдено");
                }
            }
        }
    }
}
