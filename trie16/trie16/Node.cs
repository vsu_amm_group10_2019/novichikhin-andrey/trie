﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace trie16
{
    class Node
    {
        public int num { get; set; }

        public Dictionary<char, Node> subNodes;

        public Node(int n)
        {
            num = n;
            subNodes = new Dictionary<char, Node>();
        }

        public void MakeSubTree(TreeNode treeNode)
        {
            foreach (KeyValuePair<char, Node> p in subNodes)
            {
                TreeNode n = new TreeNode(p.Key.ToString());
                p.Value.MakeSubTree(n);
                treeNode.Nodes.Add(n);
            }
        }

        public bool Delete(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return true;
            }
            char sym = word[0];
            if (subNodes.ContainsKey(sym))
            {
                if (subNodes[sym].Delete(word.Substring(1)) && subNodes[sym].subNodes.Count == 0)
                {
                    subNodes.Remove(sym);
                }
                else if (subNodes[sym].subNodes.Count != 0)
                {
                    subNodes[sym].num--;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Add(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                num++;
                return;
            }
            if (!subNodes.ContainsKey(word[0]))
            {
                subNodes.Add(word[0], new Node(0));
            }
            subNodes[word[0]].Add(word.Substring(1));
        }

        public void MakeReverse()
        {
            Stack<KeyValuePair<char, Node>> nodes = new Stack<KeyValuePair<char, Node>>();
            foreach (KeyValuePair<char, Node> p in subNodes)
            {
                nodes.Push(p);
                p.Value.MakeReverse();
            }
            subNodes.Clear();
            while (nodes.Count != 0)
            {
                KeyValuePair<char, Node> p = nodes.Pop();
                subNodes.Add(p.Key, p.Value);
            }
        }
    }
}
